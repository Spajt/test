<?php

namespace App\AppModule\Service;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ApiCommunicationServiceTest extends WebTestCase
{

    /**
     *
     */
    public function testAssertUrl()
    {
        self::bootKernel();

        $container = self::$kernel->getContainer();


        /**
         * @var ApiCommunicationService $apiCommunicationService
         */
        $apiCommunicationService = $container->get(ApiCommunicationService::class);
        $this->assertTrue((bool) filter_var($apiCommunicationService->getUrl('asdasd', ['asdasd' => 1213]), FILTER_VALIDATE_URL));
        $this->assertTrue((bool) filter_var($apiCommunicationService->getUrl('asdas??d', ['asdasd' => 1213]), FILTER_VALIDATE_URL));
        $this->assertTrue((bool) filter_var($apiCommunicationService->getUrl('asdas?=d&asdasd', ['asdasd' => 1213]), FILTER_VALIDATE_URL));
        $this->assertTrue((bool) filter_var($apiCommunicationService->getUrl('asdas?=d&asdasd', ['?asdasd&&' => 1213]), FILTER_VALIDATE_URL));


    }


    public function testBeerListService()
    {
        self::bootKernel();

        $container = self::$kernel->getContainer();


        /**
         * @var ApiCommunicationService $apiCommunicationService
         */
        $apiCommunicationService = $container->get(ApiCommunicationService::class);

        $this->assertTrue(is_array($apiCommunicationService->getBeerList()));
    }

}