<?php

namespace App\AppModule\Consumer;


use App\AppModule\Service\ImportBeerService;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;

/**
 * Class BeerConsumer
 * @package App\AppModule\Consumer
 */
class BeerConsumer implements ConsumerInterface
{

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var ImportBeerService
     */
    private $importBeerService;

    /**
     * BeerConsumer constructor.
     * @param ContainerInterface $container
     * @param LoggerInterface $logger
     */
    public function __construct(ContainerInterface $container, LoggerInterface $logger)
    {

        $this->container = $container;
        $this->logger = $logger;
        $this->importBeerService = $this->container->get(ImportBeerService::class);

    }

    /**
     * @param AMQPMessage $msg
     * @return int|mixed
     */
    public function execute(AMQPMessage $msg)
    {
        $body = $msg->getBody();

        try {
            $bodyArr = \GuzzleHttp\json_decode($body, true);
        } catch (\InvalidArgumentException $exception) {
            $this->logger->alert('Beer import invalid message format', [
                'message' => $body
            ]);

            return ConsumerInterface::MSG_REJECT_REQUEUE;
        }

        try {
            $result = $this->importBeerService->processBeer($bodyArr);
        } catch (\Throwable $exception) {
            $this->logger->alert('Importing beer exception', [
                'message' => $exception->getMessage() . PHP_EOL . $exception->getTraceAsString()
            ]);

            return ConsumerInterface::MSG_REJECT_REQUEUE;
        }


        return $result;

    }

}