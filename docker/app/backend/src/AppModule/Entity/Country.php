<?php

namespace App\AppModule\Entity;

use App\AppModule\Entity\Beer;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\AppModule\Repository\CountryRepository")
 */
class Country
{

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=3, name="country_code")
     */
    private $countryCode;


    /**
     * @ORM\OneToMany(targetEntity="App\AppModule\Entity\Brewer", mappedBy="country")
     * @ORM\JoinColumn(name="brewer_id", referencedColumnName="country_code")
     */
    private $brewers;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName() : ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Beer
     */
    public function setName(string $name) : self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCountryCode() : ?string
    {
        return $this->countryCode;
    }

    /**
     * @param string $countryCode
     * @return Country
     */
    public function setCountryCode(string $countryCode) : self
    {
        $this->countryCode = $countryCode;
        return $this;
    }



}
