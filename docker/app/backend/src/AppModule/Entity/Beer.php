<?php

namespace App\AppModule\Entity;

use App\AppModule\Traits\JsonSerializableTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\AppModule\Repository\BeerRepository")
 */
class Beer implements \JsonSerializable
{
    use JsonSerializableTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $brewerId;

    /**
     * @ORM\ManyToOne(targetEntity="App\AppModule\Entity\Brewer", inversedBy="beers")
     * @ORM\JoinColumn(name="brewer_id", referencedColumnName="id")
     */
    private $brewer;

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\Column(type="float")
     */
    private $pricePerLitre;

    /**
     * @ORM\Column(type="float")
     */
    private $price;

    /**
     * @ORM\Column(type="string")
     */
    private $type;

    /**
     * @ORM\Column(type="integer")
     */
    private $productId;


    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getBrewerId() : ?int
    {
        return $this->brewerId;
    }

    /**
     * @param mixed $brewerId
     * @return Beer
     */
    public function setBrewerId(int $brewerId) : self
    {
        $this->brewerId = $brewerId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBrewer()
    {
        return $this->brewer;
    }

    /**
     * @param mixed $brewer
     * @return Beer
     */
    public function setBrewer(Brewer $brewer) : self
    {
        $this->brewer = $brewer;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName() : ?string
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Beer
     */
    public function setName($name) : self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return float
     */
    public function getPricePerLitre() : ?float
    {
        return $this->pricePerLitre;
    }

    /**
     * @param mixed $pricePerLitre
     * @return Beer
     */
    public function setPricePerLitre(float $pricePerLitre) : self
    {
        $this->pricePerLitre = $pricePerLitre;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     * @return Beer
     */
    public function setPrice(float $price) : self
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     * @return Beer
     */
    public function setType($type) : self
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @param mixed $productId
     * @return Beer
     */
    public function setProductId($productId) : self
    {
        $this->productId = $productId;
        return $this;
    }



}
