<?php

namespace App\AppModule\Entity;

use App\AppModule\Entity\Beer;
use App\AppModule\Traits\JsonSerializableTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\AppModule\Repository\BrewerRepository")
 */
class Brewer implements \JsonSerializable
{
    use JsonSerializableTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\Column(type="string")
     * @ORM\ManyToOne(targetEntity="App\AppModule\Entity\Country", inversedBy="brewers")
     * @ORM\JoinColumn(name="country", referencedColumnName="country_code")
     */
    private $country;

    /**
     * @ORM\OneToMany(targetEntity="App\AppModule\Entity\Beer", mappedBy="brewer")
     * @ORM\JoinColumn(name="id", referencedColumnName="brewer_id")
     */
    private $beers;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName() : ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Beer
     */
    public function setName(string $name) : self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getCountry() : ?string
    {
        return $this->country;
    }

    /**
     * @param \App\Entity\Country $country
     * @return Beer
     */
    public function setCountry(string $country) : self
    {
        $this->country = $country;
        return $this;
    }

    /**
     * @return array
     */
    public function getBeers()
    {
        return $this->beers;
    }

    /**
     * @param array<Beer> $beers
     * @return Beer
     */
    public function setBeers($beers) : self
    {
        $this->beers = $beers;
        return $this;
    }


}
