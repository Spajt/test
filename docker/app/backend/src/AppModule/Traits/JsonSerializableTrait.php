<?php
namespace App\AppModule\Traits;

trait JsonSerializableTrait {

    /**
     * @return array
     */
    public function jsonSerialize()
    {

        $returnArray = [];
        $objectVars = get_object_vars($this);

        foreach ($objectVars as $key => $objectVar) {

            if (strpos($key, '__') !== FALSE) {
                continue;
            }

            if ($objectVar instanceof \JsonSerializable) {
                $returnArray[$key] = $objectVar->jsonSerialize();
            } else {
                $returnArray[$key] = $objectVar;
            }
        }

        return $returnArray;
    }
}