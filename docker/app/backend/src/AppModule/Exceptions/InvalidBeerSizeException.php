<?php

namespace App\AppModule\Exceptions;


use Throwable;

class InvalidBeerSizeException extends \Exception
{

    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct('Invalid string format', $code, $previous);
    }

}