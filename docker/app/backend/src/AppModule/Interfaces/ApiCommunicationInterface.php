<?php
namespace App\AppModule\Interfaces;

/**
 * Interface ApiCommunicationInterface
 * @package App\Interfaces
 */
interface ApiCommunicationInterface {
    function getUrl(string $endpoint, array $parameters = [] );
    function getClient();
    function sendRequest(string $endpoint, $method = 'GET', array $payload = [], array $parameters = []);
}