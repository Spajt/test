<?php
namespace App\AppModule\Interfaces;

/**
 * Interface BeerSourceInterface
 * @package App\Interfaces
 */
interface BeerSourceInterface {
    public function getBeerList();
    public function getBrewerList();
}