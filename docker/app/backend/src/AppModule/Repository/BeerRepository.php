<?php

namespace App\AppModule\Repository;

use App\AppModule\Entity\Beer;
use App\AppModule\Entity\Brewer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Beer|null find($id, $lockMode = null, $lockVersion = null)
 * @method Beer|null findOneBy(array $criteria, array $orderBy = null)
 * @method Beer[]    findAll()
 * @method Beer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BeerRepository extends ServiceEntityRepository
{
    /**
     * BeerRepository constructor.
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Beer::class);
    }

    /**
     * @param string $name
     * @param Brewer $brewer
     * @return Beer|null
     */
    public function findBeer(string $name, Brewer $brewer)
    {
        $beerEntity = $this->createQueryBuilder('n')
            ->where('n.name = :name')
            ->andWhere('n.brewerId = :brewerId')
            ->getQuery()
            ->execute([
                'name' => $name,
                'brewerId' => $brewer->getId()
            ], Query::HYDRATE_OBJECT);

        return (is_array($beerEntity) && !empty($beerEntity)) ? $beerEntity[key($beerEntity)] : $beerEntity;

    }

//    /**
//     * @return Beer[] Returns an array of Beer objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Beer
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
