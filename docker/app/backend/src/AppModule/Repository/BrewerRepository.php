<?php

namespace App\AppModule\Repository;

use App\AppModule\Entity\Brewer;
use App\AppModule\Entity\Country;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Psr\Log\LoggerInterface;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Brewer|null find($id, $lockMode = null, $lockVersion = null)
 * @method Brewer|null findOneBy(array $criteria, array $orderBy = null)
 * @method Brewer[]    findAll()
 * @method Brewer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BrewerRepository extends ServiceEntityRepository
{

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * BrewerRepository constructor.
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry, LoggerInterface $logger)
    {
        $this->logger = $logger;
        parent::__construct($registry, Brewer::class);
    }

    /**
     * @param string $name
     * @param string $countryName
     * @return \App\AppModule\Entity\Beer|Brewer|mixed
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function findOrCreateBrewer(string $name, string $countryName)
    {
        $countryRepository = $this->getEntityManager()->getRepository(Country::class);

        /**
         * @var Country $countryEntity
         */
        $countryEntity = $countryRepository->findOneBy([
            'name' => $countryName
        ]);

        $brewerEntity = $this->createQueryBuilder('n')
            ->where('n.name = :name')
            ->andWhere('n.country = :countryCode')
            ->getQuery()
            ->execute([
                'name' => $name,
                'countryCode' => $countryEntity->getCountryCode()
            ], Query::HYDRATE_OBJECT);

        if ((!is_array($brewerEntity) && !$brewerEntity instanceof Brewer) || (is_array($brewerEntity) && (empty($brewerEntity) || !$brewerEntity[key($brewerEntity)] instanceof Brewer))) {
            $this->logger->alert('creating new brewer: ' . $name);
            $brewerEntity = (new Brewer())
                ->setName($name)
                ->setCountry($countryEntity->getCountryCode());

            $this->getEntityManager()->persist($brewerEntity);
            $this->getEntityManager()->flush();
        }

        return (is_array($brewerEntity)) ? $brewerEntity[key($brewerEntity)] : $brewerEntity;
    }
}
