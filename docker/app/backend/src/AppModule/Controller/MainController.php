<?php
namespace App\AppModule\Controller;

use App\AppModule\Entity\Beer;
use App\AppModule\Service\ApiCommunicationService;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends Controller
{

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var ContainerInterface
     */
    protected $container;


    /**
     * MainController constructor.
     * @param LoggerInterface $logger
     */
    public function __construct(ContainerInterface $container, LoggerInterface $logger)
    {
        $this->container = $container;
        $this->logger = $logger;

    }

    /**
     * @Route("/beers/list/{page}/{perPage}")
     * @return JsonResponse
     */
    public function beerListAction($page = 0, $perPage = 25)
    {
        $beerRepository = $this->getDoctrine()->getRepository(Beer::class);

        $beerList = $beerRepository->findBy([],
            ['id' => 'asc'],
            $perPage,
            $page*$perPage);

        return new JsonResponse([
            'status' => 'success',
            'code' => 200,
            'data' => $beerList
        ]);
    }

}