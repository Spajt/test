<?php

namespace App\AppModule\EventListener;


use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

class ResponseListener
{

    /**
     * @param FilterResponseEvent $event
     */
    public function onResponseEvent(FilterResponseEvent $event)
    {
        $event->getResponse()->headers->set('Access-Control-Allow-Origin',  'http://localhost:4200'); //header Referer should be here
        $event->getResponse()->headers->set('Access-Control-Allow-Headers', 'content-type');
    }

}