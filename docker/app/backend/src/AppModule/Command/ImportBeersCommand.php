<?php

namespace App\AppModule\Command;


use App\AppModule\Service\ApiCommunicationService;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ImportBeersCommand extends Command
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * ImportBeersCommand constructor.
     * @param ContainerInterface $container
     * @param LoggerInterface $logger
     * @param null $name
     */
    public function __construct(ContainerInterface $container, LoggerInterface $logger, $name = null)
    {

        $this->container = $container;
        $this->logger = $logger;

        parent::__construct($name);
    }

    /**
     *
     */
    protected function configure()
    {
        $this->setName('beer-importer:import-beers')
        ->setDescription('Import beer list to rabbitmq queue');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws \Throwable
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /**
         * @var ApiCommunicationService $apiCommunicationImporter
         */
        $apiCommunicationImporter = $this->container->get(ApiCommunicationService::class);

        $beerList = $apiCommunicationImporter->getBeerList();

        $this->logger->info('Beers to push: ' . count($beerList));

        foreach ($beerList as $beer) {
            $this->container->get('old_sound_rabbit_mq.upload_beer_producer')->publish(json_encode($beer));
        }

        $this->logger->info('Beers pushed: ' . count($beerList));

    }

}