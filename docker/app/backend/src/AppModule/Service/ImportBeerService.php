<?php

namespace App\AppModule\Service;


use App\AppModule\Entity\Beer;
use App\AppModule\Entity\Brewer;
use App\AppModule\Exceptions\InvalidBeerSizeException;
use App\AppModule\Repository\BeerRepository;
use App\AppModule\Repository\BrewerRepository;
use Doctrine\ORM\EntityManagerInterface;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;

/**
 * Class ImportBeerService
 * @package App\AppModule\Service
 */
class ImportBeerService
{

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * ImportBeerService constructor.
     * @param ContainerInterface $container
     * @param LoggerInterface $logger
     */
    public function __construct(ContainerInterface $container, LoggerInterface $logger)
    {

        $this->container = $container;
        $this->logger = $logger;
    }


    /**
     * @param array $beerMsg
     * @return int
     */
    public function processBeer(array $beerMsg)
    {
        /**
         * @var BrewerRepository $brewerRepository
         */
        $brewerRepository = $this->container->get('doctrine')->getRepository(Brewer::class);

        /**
         * @var BeerRepository $beerRepository
         */
        $beerRepository = $this->container->get('doctrine')->getRepository(Beer::class);

        try {
            $brewerEntity = $brewerRepository->findOrCreateBrewer($beerMsg['brewer'], $beerMsg['country']);
            $beerEntity = $beerRepository->findBeer($beerMsg['name'], $brewerEntity);

            if (!$beerEntity instanceof Beer) {
                $beerEntity = $this->createNewBeer($beerMsg, $brewerEntity);
            }

        } catch (InvalidBeerSizeException $exception) {
            $this->logger->alert('Cannot create beer. Size is invalid');
            return ConsumerInterface::MSG_REJECT_REQUEUE;
        } catch (\Throwable $exception) {
            $this->logger->alert('Cannot create beer. Exception: ' . $exception->getMessage(), [
                'message' => $exception->getTraceAsString()
            ]);
            return ConsumerInterface::MSG_REJECT_REQUEUE;
        }

        $this->logger->alert('Acking beer: ' . $beerEntity->getName());

        return ConsumerInterface::MSG_ACK;
    }

    /**
     * @param array $beerOptions
     * @param Brewer $brewer
     * @return Beer
     * @throws InvalidBeerSizeException
     */
    public function createNewBeer(array $beerOptions, Brewer $brewer)
    {
        $beerEntity = (new Beer())
            ->setBrewer($brewer)
            ->setName($beerOptions['name'])
            ->setPricePerLitre($this->calculatePricePerLitre($beerOptions['size'], $beerOptions['price']))
            ->setType($beerOptions['type'])
            ->setPrice($beerOptions['price'])
            ->setProductId($beerOptions['product_id']);

        /**
         * @var EntityManagerInterface $em
         */
        $em = $this->container->get('doctrine')->getManager();

        $em->persist($beerEntity);
        $em->flush();

        $this->logger->info('New beer created: ' . $beerEntity->getName());

        return $beerEntity;
    }

    /**
     * @param string $sizeString
     * @param $price
     * @return float
     * @throws InvalidBeerSizeException
     */
    public function calculatePricePerLitre(string $sizeString, $price)
    {
        $stringArray = explode('×', $sizeString);
        $multiplier = trim($stringArray[0]);
        $packCapacityArray = explode(' ', str_replace(' ', ' ', $stringArray[1]));

        end($packCapacityArray);
        prev($packCapacityArray);

        $capacity = $packCapacityArray[key($packCapacityArray)];

        if (!is_numeric($multiplier) || !is_numeric($capacity) || !is_numeric($price)) {
            throw new InvalidBeerSizeException();
        }

        $pricePerLitre = number_format($price/($multiplier*$capacity) * 1000, 2);

        return floatval($pricePerLitre);
    }

}