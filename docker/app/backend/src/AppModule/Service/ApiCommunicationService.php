<?php
namespace App\AppModule\Service;


use App\AppModule\Interfaces\ApiCommunicationInterface;
use App\AppModule\Interfaces\BeerSourceInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ServerException;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;

class ApiCommunicationService implements BeerSourceInterface, ApiCommunicationInterface
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * ApiCommunicationService constructor.
     * @param ContainerInterface $container
     * @param LoggerInterface $logger
     */
    public function __construct(ContainerInterface $container, LoggerInterface $logger)
    {
        $this->logger = $logger;
        $this->container = $container;
    }

    /**
     * @return array
     * @throws \Throwable
     */
    public function getBeerList()
    {
        try {
            $this->logger->info('Getting beer list');
            $response = $this->sendRequest('beers');
        } catch (ServerException $exception) {
            $this->logger->alert('Beer list failure: ' . $exception->getMessage(), [
                'message' => $exception->getTraceAsString()
            ]);

            throw $exception;

        } catch (ClientException $exception) {
            $this->logger->alert('Beer list failure', [
                'message' => $exception->getResponse()->getBody()->getContents()
            ]);

            throw $exception;

        } catch (RequestException $exception) {
            $this->logger->alert('Beer list failure', [
                'message' => $exception->getResponse()->getBody()->getContents()
            ]);

            throw $exception;

        } catch (\Throwable $exception) {
            $this->logger->emergency('Beer list failure: ' . $exception->getMessage(), [
                'message' => $exception->getTraceAsString()
            ]);

            throw $exception;
        }

        $responseBody = $response->getBody()->getContents();

        try {
            $responseArray = \GuzzleHttp\json_decode($responseBody, true);
        } catch (\InvalidArgumentException $exception) {
            $this->logger->error('Beer list failure, response is not json', [
                'message' => $responseBody
            ]);

            throw new $exception;
        }


        return $responseArray;

    }

    public function getBrewerList()
    {
        // TODO: Implement getBrewerList() method.
    }


    /**
     * @return Client
     */
    public function getClient()
    {
        if (!$this->client instanceof Client) {
            $this->client = new Client();
        }

        return $this->client;
    }

    /**
     * @param string $endpoint
     * @param string $method
     * @param array $payload
     * @param array $parameters
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleExceptionn
     */
    public function sendRequest(string $endpoint, $method = 'GET', array $payload = [], array $parameters = [])
    {
        $url = $this->getUrl($endpoint, $payload);

        $this->logger->info('API REQUEST: ' . $url, [
            'message' => $parameters
        ]);

        return $this->getClient()->request($method, $url, $parameters);
    }

    /**
     * @param string $endpoint
     * @param array $parameters
     * @return string
     */
    public function getUrl(string $endpoint, array $parameters = [])
    {

        $endpoint = trim($endpoint, '&');
        $endpoint = trim($endpoint, '?');
        $endpoint = trim($endpoint, '/');
        $baseUrl = trim($this->container->getParameter('apiEndpoint'), '/');

        $queryString = http_build_query($parameters);


        if (!empty($queryString) && !empty($parameters)) {
            $endpoint .= ((strpos($baseUrl, '?') !== FALSE) ? '&' : '?') . $queryString;
        }

        $url = $baseUrl . '/' . $endpoint;

        return $url;
    }


}