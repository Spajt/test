-- MySQL dump 10.13  Distrib 8.0.12, for Linux (x86_64)
--
-- Host: localhost    Database: 123123
-- ------------------------------------------------------
-- Server version	8.0.12

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8mb4 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
-- MySQL dump 10.13  Distrib 8.0.12, for Linux (x86_64)
--
-- Host: localhost    Database: mydb
-- ------------------------------------------------------
-- Server version	8.0.12

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8mb4 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `beer`
--

DROP TABLE IF EXISTS `beer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `beer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `brewer_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price_per_litre` double NOT NULL,
  `price` double NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_58F666ADBCA4F952` (`brewer_id`),
  CONSTRAINT `FK_58F666ADBCA4F952` FOREIGN KEY (`brewer_id`) REFERENCES `brewer` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `beer`
--

LOCK TABLES `beer` WRITE;
/*!40000 ALTER TABLE `beer` DISABLE KEYS */;
/*!40000 ALTER TABLE `beer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `brewer`
--

DROP TABLE IF EXISTS `brewer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `brewer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `brewer`
--

LOCK TABLES `brewer` WRITE;
/*!40000 ALTER TABLE `brewer` DISABLE KEYS */;
/*!40000 ALTER TABLE `brewer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `country`
--

DROP TABLE IF EXISTS `country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_code` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `country`
--

LOCK TABLES `country` WRITE;
/*!40000 ALTER TABLE `country` DISABLE KEYS */;
/*!40000 ALTER TABLE `country` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-08-20 21:24:39


INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Afghanistan" 	,'AF');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Albania" 	,'AL');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Algeria" 	,'DZ');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Andorra" 	,'AD');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Angola" 	,'AO');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Anguilla" 	,'AI');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Antarctica" 	,'AQ');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Antigua and Barbuda" 	,'AG');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Saudi Arabia" 	,'SA');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Argentina" 	,'AR');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Armenia" 	,'AM');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Aruba" 	,'AW');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Australia" 	,'AU');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Austria" 	,'AT');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Azerbaijan" 	,'AZ');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Bahamas" 	,'BS');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Bahrain" 	,'BH');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Bangladesh" 	,'BD');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Barbados" 	,'BB');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Belgium" 	,'BE');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Belize" 	,'BZ');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Benin" 	,'BJ');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Bermuda" 	,'BM');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Bhutan" 	,'BT');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Belarus" 	,'BY');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Bolivia, Plurinational State of" 	,'BO');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Bonaire, Sint Eustatius and Saba" 	,'BQ');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Bosnia and Herzegovina" 	,'BA');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Botswana" 	,'BW');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Brazil" 	,'BR');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Brunei Darussalam" 	,'BN');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "British Indian Ocean Territory" 	,'IO');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Virgin Islands, British" 	,'VG');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Bulgaria" 	,'BG');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Burkina Faso" 	,'BF');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Burundi" 	,'BI');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Chile" 	,'CL');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "China" 	,'CN');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Croatia" 	,'HR');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Curaçao" 	,'CW');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Cyprus" 	,'CY');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Chad" 	,'TD');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Montenegro" 	,'ME');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Czechia" 	,'CZ');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "United States Minor Outlying Islands" 	,'UM');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Denmark" 	,'DK');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Congo, the Democratic Republic of the" 	,'CD');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Dominica" 	,'DM');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Dominican Republic" 	,'DO');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Djibouti" 	,'DJ');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Egypt" 	,'EG');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Ecuador" 	,'EC');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Eritrea" 	,'ER');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Estonia" 	,'EE');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Ethiopia" 	,'ET');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Falkland Islands (Malvinas)" 	,'FK');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Fiji" 	,'FJ');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Philippines" 	,'PH');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Finland" 	,'FI');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "France" 	,'FR');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "French Southern Territories" 	,'TF');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Gabon" 	,'GA');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Gambia" 	,'GM');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "South Georgia and the South Sandwich Islands" 	,'GS');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Ghana" 	,'GH');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Gibraltar" 	,'GI');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Greece" 	,'GR');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Grenada" 	,'GD');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Greenland" 	,'GL');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Georgia" 	,'GE');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Guam" 	,'GU');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Guernsey" 	,'GG');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "French Guiana" 	,'GF');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Guyana" 	,'GY');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Guadeloupe" 	,'GP');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Guatemala" 	,'GT');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Guinea-Bissau" 	,'GW');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Equatorial Guinea" 	,'GQ');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Guinea" 	,'GN');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Haiti" 	,'HT');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Spain" 	,'ES');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Netherlands" 	,'NL');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Honduras" 	,'HN');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Hong Kong" 	,'HK');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "India" 	,'IN');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Indonesia" 	,'ID');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Iraq" 	,'IQ');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Iran, Islamic Republic of" 	,'IR');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Ireland" 	,'IE');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Iceland" 	,'IS');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Israel" 	,'IL');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Jamaica" 	,'JM');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Japan" 	,'JP');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Yemen" 	,'YE');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Jersey" 	,'JE');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Jordan" 	,'JO');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Cayman Islands" 	,'KY');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Cambodia" 	,'KH');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Cameroon" 	,'CM');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Canada" 	,'CA');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Qatar" 	,'QA');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Kazakhstan" 	,'KZ');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Kenya" 	,'KE');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Kyrgyzstan" 	,'KG');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Kiribati" 	,'KI');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Colombia" 	,'CO');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Comoros" 	,'KM');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Congo" 	,'CG');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Korea, Republic of" 	,'KR');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Korea, Democratic People\'s Republic of"	,'KP');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Costa Rica"	,'CR');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Cuba"	,'CU');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Kuwait"	,'KW');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Lao People\'s Democratic Republic" 	,'LA');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Lesotho" 	,'LS');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Lebanon" 	,'LB');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Liberia" 	,'LR');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Libyan Arab Jamahiriya" 	,'LY');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Liechtenstein" 	,'LI');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Lithuania" 	,'LT');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Luxembourg" 	,'LU');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Latvia" 	,'LV');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Macedonia, the former Yugoslav Republic of" 	,'MK');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Madagascar" 	,'MG');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Mayotte" 	,'YT');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Macao" 	,'MO');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Malawi" 	,'MW');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Maldives" 	,'MV');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Malaysia" 	,'MY');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Mali" 	,'ML');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Malta" 	,'MT');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Northern Mariana Islands" 	,'MP');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Morocco" 	,'MA');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Martinique" 	,'MQ');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Mauritania" 	,'MR');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Mauritius" 	,'MU');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Mexico" 	,'MX');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Micronesia, Federated States of" 	,'FM');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Myanmar" 	,'MM');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Moldova, Republic of" 	,'MD');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Monaco" 	,'MC');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Mongolia" 	,'MN');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Montserrat" 	,'MS');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Mozambique" 	,'MZ');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Namibia" 	,'NA');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Nauru" 	,'NR');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Nepal" 	,'NP');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Germany" 	,'DE');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Niger" 	,'NE');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Nigeria" 	,'NG');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Nicaragua" 	,'NI');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Niue" 	,'NU');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Norfolk Island" 	,'NF');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Norway" 	,'NO');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "New Caledonia" 	,'NC');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "New Zealand" 	,'NZ');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Oman" 	,'OM');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Pakistan" 	,'PK');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Palau" 	,'PW');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Palestinian Territory, Occupied" 	,'PS');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Panama" 	,'PA');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Papua New Guinea" 	,'PG');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Paraguay" 	,'PY');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Peru" 	,'PE');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Pitcairn" 	,'PN');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "French Polynesia" 	,'PF');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Poland" 	,'PL');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Puerto Rico" 	,'PR');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Portugal" 	,'PT');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "South Africa" 	,'ZA');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Central African Republic" 	,'CF');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Cape Verde" 	,'CV');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Réunion" 	,'RE');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Russian Federation" 	,'RU');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Romania" 	,'RO');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Rwanda" 	,'RW');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Western Sahara" 	,'EH');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Saint Kitts and Nevis" 	,'KN');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Saint Lucia" 	,'LC');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Saint Vincent and the Grenadines" 	,'VC');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Saint Barthélemy" 	,'BL');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Saint Martin (French part)" 	,'MF');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Saint Pierre and Miquelon" 	,'PM');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "El Salvador" 	,'SV');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "American Samoa" 	,'AS');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Samoa" 	,'WS');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "San Marino" 	,'SM');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Senegal" 	,'SN');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Serbia" 	,'RS');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Seychelles" 	,'SC');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Sierra Leone" 	,'SL');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Singapore" 	,'SG');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Sint Maarten (Dutch part)" 	,'SX');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Slovakia" 	,'SK');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Slovenia" 	,'SI');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Somalia" 	,'SO');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Sri Lanka" 	,'LK');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "United States" 	,'US');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Swaziland" 	,'SZ');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Sudan" 	,'SD');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "South Sudan" 	,'SS');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Suriname" 	,'SR');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Svalbard and Jan Mayen" 	,'SJ');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Syrian Arab Republic" 	,'SY');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Switzerland" 	,'CH');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Sweden" 	,'SE');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Tajikistan" 	,'TJ');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Thailand" 	,'TH');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Taiwan, Province of China" 	,'TW');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Tanzania, United Republic of" 	,'TZ');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Timor-Leste" 	,'TL');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Togo" 	,'TG');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Tokelau" 	,'TK');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Tonga" 	,'TO');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Trinidad and Tobago" 	,'TT');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Tunisia" 	,'TN');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Turkey" 	,'TR');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Turkmenistan" 	,'TM');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Turks and Caicos Islands" 	,'TC');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Tuvalu" 	,'TV');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Uganda" 	,'UG');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Ukraine" 	,'UA');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Uruguay" 	,'UY');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Uzbekistan" 	,'UZ');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Vanuatu" 	,'VU');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Wallis and Futuna" 	,'WF');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Holy See (Vatican City State)" 	,'VA');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Venezuela, Bolivarian Republic of" 	,'VE');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Hungary" 	,'HU');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "United Kingdom" 	,'GB');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Viet Nam" 	,'VN');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Italy" 	,'IT');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Côte d\'Ivoire"	,'CI');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Bouvet Island"	,'BV');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Christmas Island"	,'CX');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Isle of Man"	,'IM');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Saint Helena, Ascension and Tristan da Cunha"	,'SH');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Åland Islands"	,'AX');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Cook Islands"	,'CK');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Virgin Islands, U.S."	,'VI');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Heard Island and McDonald Islands"	,'HM');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Cocos (Keeling) Islands"	,'CC');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Marshall Islands"	,'MH');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Faroe Islands"	,'FO');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Solomon Islands"	,'SB');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Sao Tome and Principe"	,'ST');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Zambia"	,'ZM');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "Zimbabwe"	,'ZW');
INSERT INTO `country` (`id`, `name`, `country_code`) VALUES (NULL, "United Arab Emirates"	,'AE');
