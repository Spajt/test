import { Component, OnInit } from '@angular/core';
import * as Chartist from 'chartist';
import {ApiService} from "../services/api-service";
import {Http} from "@angular/http";
//import {ApiService} from "../services/api-service";

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {


    beerList: Array<Beer>
    brewersList: Array<any>
    countryList: Array<any>
    searchString: string
    priceFrom: string
    priceTo: string


    constructor(private http: Http) { }

    ngOnInit(): void {
        this.getBeerList(0, {});
    }

    getBeerList(page: number, options: any) {
        this.http.post('http://0.0.0.0:8081/beers/list' + ((page > 0) ? '/' + page : ''), options).subscribe(data => {
            let jsonBody = data.json();
            let newList = [];
            jsonBody.data.map( item => {
                newList.push(new Beer(item.name, item.brewer.name, item.type, item.brewer.country, item.pricePerLitre))
            })

            this.beerList = newList;
        })
    }

    brewerSelect() {

    }

    countrySelect() {

    }

    typeList() {

    }

}

class Beer {

    beerName = '';
    brewer = '';
    style = '';
    country = '';
    pricePerLitre = '';

    constructor(beerName: string,brewer: string, style: string, country: string,pricePerLitre: string) {
        this.beerName = beerName;
        this.brewer = brewer;
        this.style = style;
        this.country = country;
        this.pricePerLitre = pricePerLitre;
    }


}